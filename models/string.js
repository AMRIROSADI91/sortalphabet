const mongoose = require("mongoose");

const itemSchema = new mongoose.Schema(
  {
    string: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    toJSON: { getters: true },
  }
);

module.exports = mongoose.model("item", itemSchema);
