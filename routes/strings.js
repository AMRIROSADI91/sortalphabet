// Amri's Code
const express = require("express");

// item validator
const {
  getDetailValidator,
  stringValidator,
} = require("../middlewares/validators/strings");

// Import controller
const {
  createString,
  getAllString,
  getStringById,
  updateString,
  deleteString,
} = require("../controllers/strings");

// Make router
const router = express.Router();

// Make some routes
router.post("/", stringValidator, createString);
router.get("/", getAllString);
router.get("/:id", getDetailValidator, getStringById);
router.put("/edit/:id", stringValidator, getDetailValidator, updateString);
router.delete("/:id", getDetailValidator, deleteString);

// Exports
module.exports = router;
