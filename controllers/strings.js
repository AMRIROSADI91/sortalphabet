const { string } = require("../models");

class String {
  async createString(req, res, next) {
    try {
      const newString = await string.create(req.body);

      const dataString = await string.findOne({ _id: newString._id });

      res.status(201).json({ dataString, message: "String has been created" });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  }

  async getAllString(req, res, next) {
    try {
      let data = await string.find();

      if (data.length === 0) {
        return next({ message: "strings not found", statusCode: 404 });
      }
      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async getStringById(req, res, next) {
    try {
      let data = await string.findOne({
        _id: req.params.id,
      });

      if (!data) {
        return next({ statusCode: 404, message: "String not found" });
      }

      res.status(200).json({ data, message: "String found!" });
    } catch (error) {
      next(error);
    }
  }

  async updateString(req, res, next) {
    try {
      const dataString = await string.findOneAndUpdate(
        {
          _id: req.params.id,
        },
        req.body,
        {
          new: true,
        }
      );

      if (!dataString) {
        return next({ message: "String not found", statusCode: 404 });
      }

      return res
        .status(201)
        .json({ message: "String has been successfully updated" });
    } catch (error) {
      next(error);
    }
  }

  async deleteString(req, res, next) {
    try {
      const dataString = await string.deleteById({ _id: req.params.id });

      if (dataString.n === 0) {
        return next({ message: "String not found", statusCode: 404 });
      }

      res.status(200).json({ message: "String has been deleted" });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new String();
