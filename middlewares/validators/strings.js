// Amri's Code
const { string } = require("../../models");
const validator = require("validator");
const mongoose = require("mongoose");

exports.getDetailValidator = async (req, res, next) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return next({ message: "id is not valid", statusCode: 400 });
    }

    next();
  } catch (error) {
    /* istanbul ignore next */
    next(error);
  }
};

exports.stringValidator = async (req, res, next) => {
  try {
    /* Validate the user input */
    const errorMessages = [];

    if (
      req.body.string &&
      req.body.string.split("").sort().join("") &&
      !validator.isAlpha(req.body.string)
    ) {
      errorMessages.push(`string must be alphabet`);
    }
    // if (req.body.string && req.body.string.split("").sort().join("")) {
    //   errorMessages.push("Invalid package_price format! Please insert numeric");
    // }
    // if (req.body.string) {
    //   const string = req.body.string.split("").sort().join("");

    //   errorMessages.push("Invalid package_capacity format. Example: '50-250'");
    // }
    // check for package_services validity
    // if (req.body.string && req.body.string.length > 0) {
    //   // if package_services is not an array, make it an array
    //   req.body.string.split("").sort().join("");
    // }

    if (errorMessages.length > 0) {
      return next({ statusCode: 400, messages: errorMessages });
    }

    next();
  } catch (error) {
    next(error);
  }
};
